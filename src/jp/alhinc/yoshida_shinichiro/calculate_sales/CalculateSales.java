package jp.alhinc.yoshida_shinichiro.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {
		Map<String, String> branchNames = new HashMap<>();//支店の情報を保持
		Map<String, Long> branchSales = new HashMap<>();//売上の情報を保持
		Map<String, String> commodityNames = new HashMap<>(); //商品の情報を保持
		Map<String, Long> commoditySales = new HashMap<>();//商品売上の情報を保持

		//コマンドライン引数が渡されているかチェック
		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		BufferedReader br = null;
		//売上ファイル名の取得と判定
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<File>();

		for (int i = 0; i < files.length; i++) {
			String nameSales = files[i].getName();
			if (files[i].isFile() && nameSales.matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}
		//ファイルが連番かチェック※ファイルの値ではなく名前を取って比較
		for (int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));

			if ((latter - former != 1)) {
				System.out.println("売上ファイルが連番になっていません");
				return;
			}
		}
		//売上ファイルの合算入力
		for (int i = 0; i < rcdFiles.size(); i++) {
			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);

				List<String> salesSum = new ArrayList<String>();
				String line;

				while ((line = br.readLine()) != null) {
					salesSum.add(line);
				}

				//売上ファイルのフォーマットが仕様通りかチェック
				//2行→3行に変更
				if (salesSum.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + "のフォーマットが不正です");
					return;
				}

				String branchCode = salesSum.get(0);
				String commodityCode = salesSum.get(1);

				//支店定義ファイルに該当する支店コードかチェック
				if (!branchNames.containsKey(branchCode)) {
					System.out.println(rcdFiles.get(i).getName() + "の支店コードが不正です");
					
				}

				//商品定義ファイルに該当する商品コードかチェック
				if (!commodityNames.containsKey(commodityCode)) {
					System.out.println(rcdFiles.get(i).getName() + "の商品コードが不正です");
					return;
				}

				//売上金額が数字かチェック
				if (!salesSum.get(2).matches("^[0-9]*$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				//支店の売上げ合算
				long fileSale = Long.parseLong(salesSum.get(2));
				Long salesAmount = branchSales.get(branchCode) + fileSale;

				//商品の売上げ合算
				long fileProduct = Long.parseLong(salesSum.get(2));
				Long productAmount = commoditySales.get(commodityCode) + fileProduct;

				//合算値が支店・商品共に10桁以内かチェック
				if (salesAmount >= 1000000000L && productAmount >= 1000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				branchSales.put(branchCode, salesAmount);
				commoditySales.put(branchCode, productAmount);

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;

			} finally {
				if (br != null) {
					try {
						br.close();

					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}

		//出力
		if (outFile(args[0], "branch.out", branchNames, branchSales)) {
			return;
		}
		if (outFile(args[0], "commodity.out", commodityNames, commoditySales)) {
			return;
		}

		//入力
		if(inputFile(args[0],"blanch.lst", "支店定義", "^[0-9]{3}$", branchNames, branchSales) ){
			return;
		}
		if(inputFile(args[0],"commodity.lst", "商品定義", "^[0-9a-zA-Z]{8}$", commodityNames, commoditySales)) {
			return;
		}
	}

	//支店定義ファイル読込メソッド
	private static boolean inputFile(String filePath, String fileName, String readFile,String formatFile, Map<String, String> nameMap,
			Map<String, Long> salesMap) {

		BufferedReader br = null;
		try {
			File file = new File(filePath, fileName);
			//支店(商品)定義ファイルの存在チェック
			if (!file.exists()) {
				System.out.println(readFile + "ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			//バッファした値を渡す
			String line;
			while ((line = br.readLine()) != null) {
				System.out.println(line);
				String[] items = line.split(",");//文字列の分割。支店コードと支店名(商品名）の分割

				//支店（商品）定義ファイルのフォーマットチェック
				if ((items.length != 2) || (!items[0].matches(formatFile))) {
					System.out.println(readFile+ "ファイルのフォーマットが不正です");
					return false;
				}
				nameMap.put(items[0], items[1]);//値の保持
				salesMap.put(items[0], (long) 0);// 売上高の初期値が0に設定
			}
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(br != null) {
				try {
					br.close();
				}catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	//支店別・商品別ファイルの出力メソッド
	private static boolean outFile(String filePath, String fileName, Map<String, String> nameMap,
			Map<String, Long> salesMap) {
		BufferedWriter bw = null;
		try {
			File file = new File(filePath, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for (String key : nameMap.keySet()) {
				bw.write(key + "," + nameMap.get(key) + "," + salesMap.get(key));
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.print("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}
